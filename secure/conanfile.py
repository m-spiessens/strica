from conans import ConanFile, CMake
import os

class Secure(ConanFile):
   name = "secure"
   version = "one"
   description = """
      Firmware bootstrap running in secure mode configuring access attributes.
      Ends with executing the non secure application. 
      """
   url = "https://gitlab.com/m-spiessens/strica"
   license = "MIT"
   author = "Mathias Spiessens"
   build_policy = "always"
   requires = "nrf-hal/2.4.0@nordic/stable"
   settings = { "arch": ["armv8_32"], "os": ["none"], "build_type": ["Release", "Debug"], "compiler": ["gcc"] }
   generators = "cmake"
   exports_sources = "CMakeLists.txt", "startup_gcc.c", "include/*", "source/*", "*.ld"

   def build(self):
      cmake = CMake(self, generator="Eclipse CDT4 - Unix Makefiles")
      cmake.verbose = True

      cmake.definitions["CONAN_C_FLAGS"] = "-march=armv8-m.main -mthumb"
      cmake.definitions["CONAN_CXX_FLAGS"] = "-march=armv8-m.main -mthumb -fno-exceptions"
      cmake.definitions["DEFINE_PART"] = "$DEFINE_PART"

      cmake.configure()
      cmake.build()

      self.run("rm -f arm-none-eabi-gdb; ln -s `which arm-none-eabi-gdb`")

   def package(self):
      self.copy("veneer.h", "include/", "include/")
      self.copy("veneer.o", "veneer/", "./")
      self.copy("memory.ld", "link/", "./")
      self.copy("secure.hex", "bin/", "bin/")

   def package_info(self):
      self.cpp_info.includedirs = ["include/"]
      self.cpp_info.libdirs = ["link/"]
      self.user_info.veneer = os.path.join(self.package_folder, "veneer", "veneer.o") 
      self.user_info.hex = os.path.join(self.package_folder, "bin", "secure.hex")