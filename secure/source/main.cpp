#include <assert.h>
#include <arm_cmse.h>
 
#include "nrf5340_application.h"

#include "nrf_spu.h"
#include "nrf_gpio.h"

#include "veneer.h"
 
typedef void (*void_nonsecure_call) (void) __attribute__((cmse_nonsecure_call));
 
int main(void)
{
TZ_SAU_Disable();
SAU->CTRL |= SAU_CTRL_ALLNS_Msk;

assert(nrf_spu_tz_is_available(NRF_SPU));

nrf_spu_event_clear(NRF_SPU, NRF_SPU_EVENT_RAMACCERR);
nrf_spu_event_clear(NRF_SPU, NRF_SPU_EVENT_FLASHACCERR);
nrf_spu_event_clear(NRF_SPU, NRF_SPU_EVENT_PERIPHACCERR);

nrf_spu_int_enable(NRF_SPU, NRF_SPU_INT_RAMACCERR_MASK | NRF_SPU_INT_FLASHACCERR_MASK | NRF_SPU_INT_PERIPHACCERR_MASK);

nrf_spu_flashnsc_set(NRF_SPU, 0, NRF_SPU_NSC_SIZE_4096B, 1, true);

for(unsigned int r = 0; r < 2; r++)
{
nrf_spu_flashregion_set(NRF_SPU, r, true, NRF_SPU_MEM_PERM_READ | NRF_SPU_MEM_PERM_EXECUTE, true);
}
for(unsigned int r = 2; r < 64; r++)
{
nrf_spu_flashregion_set(NRF_SPU, r, false, NRF_SPU_MEM_PERM_READ | NRF_SPU_MEM_PERM_EXECUTE, true);
}

for(unsigned int r = 1; r < 32; r++)
{
nrf_spu_ramregion_set(NRF_SPU, r, false, NRF_SPU_MEM_PERM_READ | NRF_SPU_MEM_PERM_WRITE | NRF_SPU_MEM_PERM_EXECUTE, true);
}

nrf_spu_peripheral_set(NRF_SPU, NRFX_PERIPHERAL_ID_GET(NRF_TIMER0), false, false, true);
// nrf_spu_peripheral_set(NRF_SPU, NRFX_PERIPHERAL_ID_GET(NRF_P0), false, false, true);
nrf_spu_gpio_config_set(NRF_SPU, 0, 0x00000000, true);

for (uint8_t i = 0; i < sizeof(NVIC->ITNS) / sizeof(NVIC->ITNS[0]); i++) {
NVIC->ITNS[i] = 0xFFFFFFFF;
}

/* Make sure that the SPU is targeted to S state */
NVIC_ClearTargetState((IRQn_Type)NRFX_IRQ_NUMBER_GET(NRF_SPU));

NVIC_SetPriority(SecureFault_IRQn, 0);
SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk
  | SCB_SHCSR_BUSFAULTENA_Msk
  | SCB_SHCSR_MEMFAULTENA_Msk
  | SCB_SHCSR_SECUREFAULTENA_Msk;

NVIC_ClearPendingIRQ((IRQn_Type)NRFX_IRQ_NUMBER_GET(NRF_SPU));
NVIC_EnableIRQ((IRQn_Type)NRFX_IRQ_NUMBER_GET(NRF_SPU));

extern char __ns_application __asm("_ns_application"); // See linker script.
uint32_t* ns_application = (uint32_t*)&__ns_application;

/* Set non-secure main stack (MSP_NS) */
__TZ_set_MSP_NS(ns_application[0]);

/* Get non-secure reset handler */
void_nonsecure_call application = (void_nonsecure_call)(ns_application[1]);

__DSB();
__ISB();

/* Start non-secure state software application */
application();

/* Non-secure software does not return, this code is not executed */
while(true);
}

extern "C" void faultSecure(void)
{
if(nrf_spu_event_check(NRF_SPU, NRF_SPU_EVENT_RAMACCERR))
{
nrf_spu_event_clear(NRF_SPU, NRF_SPU_EVENT_RAMACCERR);
}
if(nrf_spu_event_check(NRF_SPU, NRF_SPU_EVENT_FLASHACCERR))
{
nrf_spu_event_clear(NRF_SPU, NRF_SPU_EVENT_FLASHACCERR);
}
if(nrf_spu_event_check(NRF_SPU, NRF_SPU_EVENT_PERIPHACCERR))
{
nrf_spu_event_clear(NRF_SPU, NRF_SPU_EVENT_PERIPHACCERR);
}
__asm volatile("bkpt");
while(true);
}

void __attribute__((cmse_nonsecure_entry)) enableLed()
{
  nrf_gpio_cfg_output(31);
}

void __attribute__((cmse_nonsecure_entry)) toggleLedPlease()
{
	nrf_gpio_pin_toggle(31);
}
